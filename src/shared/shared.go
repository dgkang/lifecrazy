package shared

import (
	"bufio"
	"bytes"
	proto "code.google.com/p/goprotobuf/proto"
	"container/list"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/fzzy/radix/redis"
	"github.com/wendal/goyaml2"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"path"
	"runtime"
	"time"
)

const (
	ProtocolHeaderLength = 8

	P_AcquireUserData = 0x01
	P_BuyAsset        = 0x02
	P_UpdateAsset     = 0x03
	P_Attack          = 0x04
	P_AcquireRanking  = 0x05

	ERROR = 0
	INFO  = 1
	WARN  = 2
	DEBUG = 3
)

func LoadConf(file string, conf interface{}) error {

	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}

	err = json.Unmarshal(b, &conf)
	if err != nil {
		return err
	}
	return nil
}

type YAMLConfigContainer struct {
	data map[string]interface{}
}

func NewYaml(path string) (*YAMLConfigContainer, error) {
	y := &YAMLConfigContainer{data: make(map[string]interface{})}
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	buf, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	if len(buf) < 3 {
		return nil, errors.New("len(buf) < 3")
	}

	_map, _err := goyaml2.Read(bytes.NewBuffer(buf))
	if _err != nil {
		return nil, _err
	}

	if _map == nil {
		return nil, errors.New("Goyaml2 Read failed.")
	}

	r, ok := _map.(map[string]interface{})
	if !ok {
		return nil, errors.New("Result not a map")
	}
	y.data = r
	return y, nil
}

func (y *YAMLConfigContainer) Int(key string, DefaultInt int) (int, error) {
	if v, ok := y.data[key].(int64); ok {
		return int(v), nil
	}
	return DefaultInt, errors.New("not int value")
}

func (y *YAMLConfigContainer) String(key string) string {
	if v, ok := y.data[key].(string); ok {
		return v
	}
	return ""
}

type Logging struct {
	*log.Logger
	baseLevel int
}

func (l *Logging) Printf(level int, format string, v ...interface{}) error {
	sFlag := [...]string{"ERROR", " INFO", " WARN", "DEBUG"}

	if level < 0 || level >= len(sFlag) {
		return fmt.Errorf("bad level value,%d", level)
	}
	s := fmt.Sprintf(format, v...)

	_, file, line, _ := runtime.Caller(1)
	base := path.Base(file)
	l.Logger.Printf("[%s] [%s,%d] %s", sFlag[level], base, line, s)
	return nil
}

func NewLogging(LogName string, baseLevel int) (*Logging, error) {
	f, e := os.OpenFile(LogName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if e != nil {
		return nil, e
	}
	return &Logging{log.New(f, "", log.LstdFlags), baseLevel}, nil
}

//struct RedisSession
type RedisSession struct {
	RedisConn *redis.Client
	RedisAddr string
	Log       *Logging
}

type RedisConnectionPool struct {
	Sessions []*RedisSession
	MaxConn  int
	WChan    chan *RedisSession
	RChan    chan *RedisSession
	InitChan chan error
	Host     string
	Port     int
	L        *Logging
}

func NewRedisConnectionPool(maxConn int, host string, port int, l *Logging) *RedisConnectionPool {
	return &RedisConnectionPool{
		Sessions: make([]*RedisSession, maxConn),
		MaxConn:  maxConn,
		WChan:    make(chan *RedisSession),
		RChan:    make(chan *RedisSession),
		InitChan: make(chan error),
		Host:     host,
		Port:     port,
		L:        l}
}

func (r *RedisConnectionPool) Run() {
	freeList := list.New()
	freeList.Init()
	var e error

	for i := 0; i < len(r.Sessions); i++ {
		r.Sessions[i] = NewRedisSession(r.Host, r.Port, r.L)
		e = r.Sessions[i].Connect()
		if e != nil {
			break
		}
		freeList.PushBack(r.Sessions[i])
		r.L.Printf(INFO, "[%d]Connect redis server %s", i+1, r.Sessions[i].RedisAddr)
	}
	r.InitChan <- e
	if e != nil {
		return
	}
	r.L.Printf(INFO, "RedisConnectionPool ready ok,Number of pool is %d", len(r.Sessions))

	var t *RedisSession = nil
	for {
		e := freeList.Front()
		if e != nil {
			t = e.Value.(*RedisSession)
		} else {
			t = nil
		}

		select {
		case r.WChan <- t:
			if t != nil {
				freeList.Remove(e)
			}
			//		r.L.Printf(DEBUG, "Allocation RedisSession,%d",freeList.Len())

		case s := <-r.RChan:
			freeList.PushBack(s)
			//		r.L.Printf(DEBUG,"Recycle RedisSession,%d",freeList.Len())
		}
	}
}

func (r *RedisConnectionPool) Init() error {
	go r.Run()
	e := <-r.InitChan
	return e
}

func (r *RedisConnectionPool) Acquire() *RedisSession {
	for {
		session := <-r.WChan
		if session == nil {
			time.Sleep(time.Millisecond * 30)
			r.L.Printf(WARN, "Wait for get session,30 msec")
		} else {
			return session
		}
	}
	return nil
}

func (r *RedisConnectionPool) Release(s *RedisSession) {
	if s != nil {
		r.RChan <- s
	} else {
		r.L.Printf(ERROR, "PutSession  nil.")
	}
}

func NewRedisSession(host string, port int, l *Logging) *RedisSession {
	s := new(RedisSession)
	s.RedisAddr = fmt.Sprintf("%s:%d", host, port)
	s.Log = l
	return s
}

func (s *RedisSession) Connect() error {
	c, e := redis.Dial("tcp", s.RedisAddr)
	if e != nil {
		return fmt.Errorf("Connect redis server %s failed,%s ", s.RedisAddr, e.Error())
	}
	s.RedisConn = c
	return nil
}

func (s *RedisSession) SendCommand(commandName string, args ...interface{}) *redis.Reply {
	var r *redis.Reply
	for i := 0; i < 3; i++ {
		r = s.RedisConn.Cmd(commandName, args...)
		if r.Err != nil {
			s.Log.Printf(ERROR, "redis connection error,%v", r.Err)
			s.Close()
			s.Connect()
		} else {
			break
		}
	}
	return r
}

func (s *RedisSession) Close() {
	s.RedisConn.Close()
}

//struct Connection

type Connection struct {
	Conn net.Conn
	Cb   func(*Connection, int, int, *bytes.Buffer) bool
	Log  *Logging
}

func (c *Connection) SendMessage(pb proto.Message, opcode int32) bool {
	b, er := proto.Marshal(pb)
	if er != nil {
		c.Log.Printf(ERROR, "Proto.Marshal() error,%s", er.Error())
		return false
	}

	buf := GenerateProtoPackage(c.Log, b, opcode)
	if buf == nil {
		return false
	}

	w := bufio.NewWriter(c.Conn)
	_, e := w.Write(buf)
	if e != nil {
		c.Log.Printf(ERROR, "Socket send data error,%s", e.Error())
		return false
	}
	w.Flush()
	return true
}
func (c *Connection) Close() {
	c.Conn.Close()
	c.Log.Printf(INFO, "Close Socket %v", c.Conn.RemoteAddr())
}
func ConnectionHandle(c *Connection) bool {
	defer func() {
		if err := recover(); err != nil {
			c.Log.Printf(ERROR, "Exception: %v", err)
			for i := 1; ; i++ {
				_, file, line, ok := runtime.Caller(i)
				if file != "" {
					c.Log.Printf(ERROR, "Stack: %s,%d", file, line)
				}
				if !ok {
					break
				}
			}
		}
	}()

	rbuf := &bytes.Buffer{}
	//rio:
	for {
		var data [1024]byte

		n, e := c.Conn.Read(data[0:])
		if e != nil {
			if e == io.EOF {
				c.Log.Printf(DEBUG, "%v close connection", c.Conn.RemoteAddr())
			} else {
				c.Log.Printf(ERROR, "Socket read error,%s", e.Error())
				c.Close()
			}
			break
		}
		rbuf.Write(data[0:n])

		for rbuf.Len() >= ProtocolHeaderLength {

			header := &bytes.Buffer{}
			header.Write(rbuf.Bytes()[0:8])

			var datalen int32
			e = binary.Read(header, binary.LittleEndian, &datalen)
			if e != nil {
				c.Log.Printf(ERROR, "Binary.Read datalen error,%s\n", e.Error())
				c.Close()
				return false
			}

			var opcode int32
			e = binary.Read(header, binary.LittleEndian, &opcode)
			if e != nil {
				c.Log.Printf(ERROR, "Binary.Read opcode error,%s\n", e.Error())
				c.Close()
				return false
			}

			if rbuf.Len() >= int(datalen)+int(ProtocolHeaderLength) {
				b := [ProtocolHeaderLength]byte{}
				rbuf.Read(b[0:])

				if !c.Cb(c, int(opcode), int(datalen), rbuf) {
					c.Log.Printf(ERROR, "Process opcode: %d,failed", opcode)
				}
			} else {
				break
			}
		}
	}
	return true
}

func GenerateProtoPackage(l *Logging, payload []byte, opcode int32) []byte {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.LittleEndian, int32(len(payload)))
	if err != nil {
		l.Fatalf("binary.Write failed:", err)
		return nil
	}
	err = binary.Write(buf, binary.LittleEndian, opcode)
	if err != nil {
		l.Fatalf("binary.Write failed:", err)
		return nil
	}
	buf.Write(payload)
	return buf.Bytes()
}
