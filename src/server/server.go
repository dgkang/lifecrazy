package main

import (
	"NetWorkProto"
	"bytes"
	proto "code.google.com/p/goprotobuf/proto"
	"fmt"
	"math"
	"net"
	"runtime"
	"shared"
	"strconv"
	"sync"
	"time"
)

const (
	redisServerIP    = "localhost"
	redisServerPort  = 6379
	DefaultPort      = 8484
	DefaultHost      = "0.0.0.0"
	S_DeviceID       = "S:DeviceID"
	H_Player         = "H:Player:%s"
	E                = 2.718281828
	AssetNumber      = 10
	SaveDataInterval = 10
	DefaultCPUCore   = 1
)

type ServerConf struct {
	ListenHost      string
	ListenPort      int
	RedisHost       string
	RedisPort       int
	NumberOfCPUCore int
}

type AssetBase struct {
	Name string
	Base float64
	BuyX float64
	BuyY float64

	UpdateX float64
	UpdateY float64

	InitNumber int
}

type BossBase struct {
	HpX float64
	HpY float64
}

type Assert struct {
	Number uint64
	Level  uint64
}

type Player struct {
	ID   string
	Name string

	Coins       float64
	CoinsPerSec float64

	Asserts [AssetNumber]Assert

	BossHP     float64
	InitBossHP float64
	BossLevel  uint64

	AttackPower float64

	M             sync.Mutex
	LastSaveRedis int64
	LastCalc      int64
}

var AssetBaseInfo = [AssetNumber]AssetBase{
	{"A", 0.1, 15.318, 0.1387, 1357.2, 1.1513, 0},
	{"B", 0.8, 83.772, 0.1501, 10587.6, 1.1627, 0},
	{"C", 4, 435.02, 0.1397, 52938, 1.1523, 0},
	{"D", 10, 2609, 0.1398, 132345, 1.1524, 0},
	{"E", 40, 9608.7, 0.1438, 529380, 1.1564, 0},
	{"F", 100, 34435, 0.1398, 0, 0, 0},
	{"G", 400, 172174, 0.1398, 0, 0, 0},
	{"H", 6666, 1434782.67, 0.14, 0, 0, 0},
	{"I", 6666, 106280192.93, 0.14, 0, 0, 0},
	{"J", 4399992, 3443478260.50, 0.14, 0, 0, 0}}

var BossBaseInfo = [...]BossBase{
	{321.13, 0.1497},
	{9521.7, 0.1138},
	{1434782.67, 0.14}}

var logFile *shared.Logging
var redisConnPool *shared.RedisConnectionPool
var Players map[string]*Player
var SortPlayers []*Player
var PlayersMutex sync.Mutex

func AcquireUserData(cs *shared.Connection, b []byte) bool {
	reqData := &NetWorkProto.AcquireUserData{}
	err := proto.Unmarshal(b, reqData)
	if err != nil {
		cs.Log.Printf(shared.ERROR, "proto.Unmarshaling error: ", err.Error())
		return false
	}

	var P *Player
	var ok bool

	PlayersMutex.Lock()
	if P, ok = Players[*reqData.DeviceID]; ok == false {
		P = NewPlayer(*reqData.DeviceID, true)
		P.Name = *reqData.DeviceName
		Players[P.ID] = P
		SortPlayers = append(SortPlayers, P)
		SavePlayer(P)
	}
	PlayersMutex.Unlock()

	P.M.Lock()
	respond := new(NetWorkProto.ReplyUserData)
	respond.DeviceID = proto.String(P.ID)
	respond.NumberOfCoin = proto.Float64(P.Coins)
	respond.CoinPerSecond = proto.Float64(P.CoinsPerSec)
	respond.BossHP = proto.Float64(P.BossHP)
	respond.InitBossHP = proto.Float64(P.InitBossHP)
	respond.BossLevel = proto.Uint64(P.BossLevel)

	for i := 0; i < AssetNumber; i++ {
		a := new(NetWorkProto.Asset)
		a.Type = NetWorkProto.AssetType(i).Enum()
		a.Level = proto.Uint64(P.Asserts[i].Level)
		a.Count = proto.Uint64(P.Asserts[i].Number)
		respond.Assets = append(respond.Assets, a)
	}
	P.M.Unlock()

	cs.Log.Printf(shared.DEBUG, "ReplyUserData: %s", respond.String())
	return cs.SendMessage(respond, int32(shared.P_AcquireUserData))
}

func UpdateAsset(cs *shared.Connection, b []byte) bool {
	reqData := &NetWorkProto.UpdateAsset{}
	err := proto.Unmarshal(b, reqData)
	if err != nil {
		cs.Log.Printf(shared.ERROR, "proto.Unmarshaling error: ", err.Error())
		return false
	}

	var P *Player
	var ok bool
	PlayersMutex.Lock()
	if P, ok = Players[*reqData.DeviceID]; ok {
		PlayersMutex.Unlock()
		P.M.Lock()
		nIndex := *reqData.Type

		N := *reqData.AddLevel
		for N > 0 {
			d := AssetBaseInfo[nIndex].UpdateX * math.Pow(E, AssetBaseInfo[nIndex].UpdateY*float64(P.Asserts[nIndex].Level))
			if P.Coins >= d {
				P.Coins -= d
				P.Asserts[nIndex].Level++
			} else {
				break
			}
			N--
		}
		P.M.Unlock()
	} else {
		PlayersMutex.Unlock()
	}
	return true
}

func BuyAsset(cs *shared.Connection, b []byte) bool {
	reqData := &NetWorkProto.BuyAsset{}
	err := proto.Unmarshal(b, reqData)
	if err != nil {
		cs.Log.Printf(shared.ERROR, "proto.Unmarshaling error: ", err.Error())
		return false
	}

	var P *Player
	var ok bool
	PlayersMutex.Lock()
	if P, ok = Players[*reqData.DeviceID]; ok {
		PlayersMutex.Unlock()
		P.M.Lock()
		nIndex := *reqData.Type

		N := *reqData.NumberofAssert
		for N > 0 {
			d := AssetBaseInfo[nIndex].BuyX * math.Pow(E, AssetBaseInfo[nIndex].BuyY*float64(P.Asserts[nIndex].Number))
			if P.Coins >= d {
				P.Coins -= d
				P.Asserts[nIndex].Number++
			} else {
				break
			}
			N--
		}
		P.M.Unlock()
	} else {
		PlayersMutex.Unlock()
	}
	return true
}

func AttackBoss(cs *shared.Connection, b []byte) bool {
	reqData := &NetWorkProto.AttackBoss{}
	err := proto.Unmarshal(b, reqData)
	if err != nil {
		cs.Log.Printf(shared.ERROR, "proto.Unmarshaling error: ", err.Error())
		return false
	}

	var P *Player
	var ok bool
	PlayersMutex.Lock()
	if P, ok = Players[*reqData.DeviceID]; ok {
		PlayersMutex.Unlock()

		P.M.Lock()
		d := float64(P.Asserts[0].Level)
		P.Coins += d
		if P.BossHP > d {
			P.BossHP -= d
		} else {
			P.BossLevel++
			CalcBossHP(P)
		}
		P.M.Unlock()
	} else {
		PlayersMutex.Unlock()
	}

	return true
}

func AcquireRanking(cs *shared.Connection, b []byte) bool {
	reqData := &NetWorkProto.AcquireRanking{}
	err := proto.Unmarshal(b, reqData)
	if err != nil {
		cs.Log.Printf(shared.ERROR, "proto.Unmarshaling error: ", err.Error())
		return false
	}

	return true
}

func ServerProctocol(cs *shared.Connection, opcode int, protoLen int, b *bytes.Buffer) bool {
	buf := make([]byte, protoLen)
	b.Read(buf[0:])

	var r bool = true
	switch opcode {

	case shared.P_AcquireUserData:
		r = AcquireUserData(cs, buf)

	case shared.P_BuyAsset:
		r = BuyAsset(cs, buf)

	case shared.P_UpdateAsset:
		r = UpdateAsset(cs, buf)

	case shared.P_Attack:
		r = AttackBoss(cs, buf)

	case shared.P_AcquireRanking:
		r = AcquireRanking(cs, buf)

	default:
		cs.Log.Printf(shared.ERROR, "Invaild opcode:%d", opcode)
	}
	return r
}

func ServerInit() (*ServerConf, error) {
	var e error

	logFile, e = shared.NewLogging("log", shared.INFO)
	if e != nil {
		return nil, fmt.Errorf("Create log file error,%s", e.Error())
	}

	conf := &ServerConf{RedisHost: redisServerIP, RedisPort: redisServerPort,
		ListenPort: DefaultPort, ListenHost: DefaultHost}

	var yaml *shared.YAMLConfigContainer
	if yaml, e = shared.NewYaml("server.conf"); e != nil {
		return nil, fmt.Errorf("Load server.conf failed,%s", e.Error())

	}
	conf.ListenHost = yaml.String("listenhost")
	conf.ListenPort, e = yaml.Int("listenport", DefaultPort)
	conf.RedisHost = yaml.String("redishost")
	conf.RedisPort, e = yaml.Int("redisport", redisServerPort)
	conf.NumberOfCPUCore, e = yaml.Int("cpu", DefaultCPUCore)

	AssetBaseInfo[0].InitNumber, e = yaml.Int("ANum", 0)
	AssetBaseInfo[1].InitNumber, e = yaml.Int("BNum", 0)
	AssetBaseInfo[2].InitNumber, e = yaml.Int("CNum", 0)
	AssetBaseInfo[3].InitNumber, e = yaml.Int("DNum", 0)
	AssetBaseInfo[4].InitNumber, e = yaml.Int("ENum", 0)
	AssetBaseInfo[5].InitNumber, e = yaml.Int("FNum", 0)
	AssetBaseInfo[6].InitNumber, e = yaml.Int("GNum", 0)
	AssetBaseInfo[7].InitNumber, e = yaml.Int("HNum", 0)
	AssetBaseInfo[8].InitNumber, e = yaml.Int("INum", 0)
	AssetBaseInfo[9].InitNumber, e = yaml.Int("JNum", 0)

	runtime.GOMAXPROCS(conf.NumberOfCPUCore)

	redisConnPool = shared.NewRedisConnectionPool(2, conf.RedisHost, int(conf.RedisPort), logFile)
	if e = redisConnPool.Init(); e != nil {
		return nil, fmt.Errorf("redis Pool init faild,%s", e.Error())
	}

	Players = make(map[string]*Player)
	SortPlayers = make([]*Player, 0)
	return conf, nil
}

func CalcBossHP(P *Player) {
	if P.BossLevel >= 1 && P.BossLevel <= 10 {
		P.BossHP = BossBaseInfo[0].HpX * math.Pow(E, BossBaseInfo[0].HpY)
	} else if P.BossLevel > 10 && P.BossLevel <= 20 {
		P.BossHP = BossBaseInfo[1].HpX * math.Pow(E, BossBaseInfo[1].HpY)
	} else {
		P.BossHP = BossBaseInfo[2].HpX * math.Pow(E, BossBaseInfo[2].HpY)
	}
	P.InitBossHP = P.BossHP
}

func NewPlayer(id string, bSet bool) *Player {
	P := new(Player)
	P.ID = id
	for i := 0; i < AssetNumber; i++ {
		P.Asserts[i].Level = 1
		P.Asserts[i].Number = uint64(AssetBaseInfo[i].InitNumber)
	}
	P.AttackPower = 1.0
	P.BossLevel = 1
	CalcBossHP(P)

	if bSet {
		redisSession := redisConnPool.Acquire()
		r := redisSession.SendCommand("SADD", S_DeviceID, id)
		redisConnPool.Release(redisSession)
		if r.Err != nil {
			logFile.Printf(shared.DEBUG, "SADD \"%s\" failed,%s", id, r.Err.Error())
		}
	}
	return P
}

func ReadPlayer(P *Player) error {
	redisSession := redisConnPool.Acquire()
	defer func() {
		redisConnPool.Release(redisSession)
		P.M.Unlock()
	}()
	P.M.Lock()

	Key := fmt.Sprintf(H_Player, P.ID)

	Values := make(map[string]string)
	var e error
	r := redisSession.SendCommand("HGETALL", Key)
	if r == nil {
		return fmt.Errorf("ReadPlayer,ID: %s,Failed,%s", P.ID)
	} else {
		if Values, e = r.Hash(); e != nil {
			return fmt.Errorf("ReadPlayer,ID: %s,Failed,%s", P.ID, e.Error())
		}
	}

	if v, ok := Values["Name"]; ok {
		P.Name = v
	}

	if v, ok := Values["Coins"]; ok {
		if Float64, er := strconv.ParseFloat(v, 64); er == nil {
			P.Coins = Float64
		}
	}

	if v, ok := Values["CoinsPerSec"]; ok {
		if Float64, er := strconv.ParseFloat(v, 64); er == nil {
			P.CoinsPerSec = Float64
		}
	}

	if v, ok := Values["BossLevel"]; ok {
		if UInt64, er := strconv.ParseUint(v, 10, 64); er == nil {
			P.BossLevel = UInt64
		}
	}

	if v, ok := Values["BossHP"]; ok {
		if Float64, er := strconv.ParseFloat(v, 64); er == nil {
			P.BossHP = Float64
		}
	}

	for i := 0; i < AssetNumber; i++ {
		if v, ok := Values[AssetBaseInfo[i].Name+"Num"]; ok {
			if UInt64, er := strconv.ParseUint(v, 10, 64); er == nil {
				P.Asserts[i].Number = UInt64
			}
		}
	}

	for i := 0; i < 5; i++ {
		if v, ok := Values[AssetBaseInfo[i].Name+"Level"]; ok {
			if UInt64, er := strconv.ParseUint(v, 10, 64); er == nil {
				P.Asserts[i].Level = UInt64
			}
		}
	}
	return nil
}

func LoadPlayers() error {
	redisSession := redisConnPool.Acquire()

	r := redisSession.SendCommand("SMEMBERS", S_DeviceID)
	l, e := r.List()
	if e != nil {
		return fmt.Errorf("SendCommand(\"SMEMBERS\",S_DeviceID) Failed,%s", e.Error())
	}
	redisConnPool.Release(redisSession)

	nIndex := 0
	for _, v := range l {
		P := NewPlayer(v, false)
		if e := ReadPlayer(P); e != nil {
			logFile.Printf(shared.ERROR, "%s", e.Error())
		} else {
			PlayersMutex.Lock()
			Players[v] = P
			SortPlayers = append(SortPlayers, P)
			PlayersMutex.Unlock()
			nIndex++
			logFile.Printf(shared.DEBUG, "%d: %v", nIndex, P)
		}
	}
	logFile.Printf(shared.DEBUG, "MapTotal: %d,SortArray: %d", len(Players), len(SortPlayers))
	return nil
}

func SavePlayer(P *Player) error {
	redisSession := redisConnPool.Acquire()
	defer func() {
		redisConnPool.Release(redisSession)
		P.M.Unlock()
	}()
	P.M.Lock()

	Key := fmt.Sprintf(H_Player, P.ID)
	r := redisSession.SendCommand("HMSET", Key, "Name", P.Name,
		"Coins", P.Coins, "CoinsPerSec", P.CoinsPerSec, "BossHP", P.BossHP, "BossLevel", P.BossLevel,
		AssetBaseInfo[0].Name+"Num", P.Asserts[0].Number,
		AssetBaseInfo[1].Name+"Num", P.Asserts[1].Number,
		AssetBaseInfo[2].Name+"Num", P.Asserts[2].Number,
		AssetBaseInfo[3].Name+"Num", P.Asserts[3].Number,
		AssetBaseInfo[4].Name+"Num", P.Asserts[4].Number,
		AssetBaseInfo[5].Name+"Num", P.Asserts[5].Number,
		AssetBaseInfo[6].Name+"Num", P.Asserts[6].Number,
		AssetBaseInfo[7].Name+"Num", P.Asserts[7].Number,
		AssetBaseInfo[8].Name+"Num", P.Asserts[8].Number,
		AssetBaseInfo[9].Name+"Num", P.Asserts[9].Number,
		AssetBaseInfo[0].Name+"Level", P.Asserts[0].Level,
		AssetBaseInfo[1].Name+"Level", P.Asserts[1].Level,
		AssetBaseInfo[2].Name+"Level", P.Asserts[2].Level,
		AssetBaseInfo[3].Name+"Level", P.Asserts[3].Level,
		AssetBaseInfo[4].Name+"Level", P.Asserts[4].Level)

	if r == nil || r.Err != nil {
		return fmt.Errorf("SavePlayer ID: %s failed,%v", P.ID, r.Err)
	}
	return nil
}

func ProcessPlayer(P *Player, tNow int64) {
	P.M.Lock()

	defer func() {
		P.M.Unlock()
	}()

	var PerCoins float64
	for i := 0; i < AssetNumber; i++ {
		PerCoins += float64(P.Asserts[i].Number) * AssetBaseInfo[i].Base * float64(P.Asserts[i].Level)
	}
	P.CoinsPerSec = PerCoins
	var d uint64 = 1
	if P.LastCalc != 0 {
		d = uint64(tNow - P.LastCalc)
	}
	P.Coins += float64(d) * PerCoins

	if P.BossHP > float64(d)*PerCoins/10.0 {
		P.BossHP -= float64(d) * PerCoins / 10.0
	} else {
		P.BossLevel++
		CalcBossHP(P)
	}
	P.LastCalc = tNow
}

func CalcCoinAndSave() {
	for {
		tNow := time.Now()
		for _, v := range Players {
			ProcessPlayer(v, tNow.Unix())
			if tNow.Unix()-v.LastSaveRedis >= SaveDataInterval {
				v.LastSaveRedis = tNow.Unix()
				SavePlayer(v)
			}
		}
		time.Sleep(time.Second * 1)
	}
}

func main() {
	var e error
	var conf *ServerConf

	if conf, e = ServerInit(); e != nil {
		logFile.Printf(shared.ERROR, "Init server failed,%s", e.Error())
		return
	}

	if e = LoadPlayers(); e != nil {
		logFile.Printf(shared.DEBUG, "LoadPlayer failed,%s", e.Error())
		return
	}

	go CalcCoinAndSave()

	a := fmt.Sprintf("%s:%d", conf.ListenHost, int(conf.ListenPort))
	addr, err := net.ResolveTCPAddr("tcp4", a)
	if err != nil {
		logFile.Printf(shared.ERROR, "Server start failed,%s", err.Error())
		return
	}
	listener, err := net.ListenTCP("tcp", addr)
	if err != nil {
		logFile.Printf(shared.ERROR, "Server start failed,%s", err.Error())
		return
	}
	logFile.Printf(shared.INFO, "The server is now ready to accept connections on %s,cpus: %d", a, runtime.GOMAXPROCS(0))

	for {
		conn, err := listener.Accept()
		if err != nil {
			logFile.Printf(shared.ERROR, "Accept error,%s", err.Error())
			listener.Close()
			return
		}
		cs := &shared.Connection{Conn: conn, Cb: ServerProctocol, Log: logFile}
		go shared.ConnectionHandle(cs)
	}
}
